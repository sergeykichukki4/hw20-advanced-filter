"use strict";

const vehicles = [
    {
        name: 'Ford',
        location: 'Ukraine',
        description: 'Ukraine',
        locales: {
            name: 'Maserati',
            description:
                {name: 'Toyota'},
        },
    },
    {
        name: 'Toyota',
        location: 'Ukraine',
        description: 'en_US'
    },
    {
        name: 'BMW',
        description: 'en_US',
        contentType: [
            {
                description: {name: 'Ford'},
            },
            {
                name: 'Toyota',
            },
        ]
    },
]

const createNormalizedKeyFields = function (argsArr) {
    return argsArr.map(keyField => {
        return keyField.split('.');
    })
}

function checkObjAccordance(keyWords, element, objParams, flag, switcher) {
    if (element) {
        if (Array.isArray(element)) {
            for (let obj of element) {
                return checkObjAccordance(keyWords, obj, objParams, flag, switcher);
            }
        }
        if (objParams.length > 1) {
            const [firstParam, ...rest] = objParams;
            const newElement = element[firstParam];
            return checkObjAccordance(keyWords, newElement, rest, flag, switcher);
        }
        for (let word of keyWords) {
            if (element[objParams[0]].toLowerCase() === word.toLowerCase()) {
                if (!flag) {
                    switcher = keyWords.length;
                    break;
                }
                switcher++;
            }
        }
    }

    return switcher;
}

const filterCollection = (arr, stringOfKeyWords, flag, ...args) => {
    const keyFields = createNormalizedKeyFields(args);
    const keyWords = stringOfKeyWords.split(' ');
    return arr.filter(element => {
        let switcher = 0;
        keyFields.forEach(arg => {
            switcher = checkObjAccordance(keyWords, element, arg, flag, switcher);
        })
        return switcher === keyWords.length;
    });
}

console.log(filterCollection(vehicles,'ford en_uS', true, 'name', 'description', 'contentType.description.name', 'locales.name', 'locales.description.name'));


